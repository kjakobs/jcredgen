
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/*
Copyright 2017 Kay Jakobs

This file is part of jCredGen.

jCredGen is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

jCredGen is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Main extends Application {
    
    String version = "0.9";
    
    int id;
    String pw;
    
    int uiWidth = 304;
    int uiHeight = 520;
    //calculated size relating to phones resolution
    //int uiHeight = 540;
    
    int idLength = 6;
    int pwLength = 8;
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Credentials Generator");
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("jCredGen.png"));
        Group root = new Group();
        Scene scene = new Scene(root, uiWidth, uiHeight, Color.WHITE);
        scene.getStylesheets().add("style.css");
        
        TabPane tabPane = new TabPane();
        BorderPane borderPane = new BorderPane();
        
        int gap = 10;
        int tGap = 30;
        int vGap = 25;
        int iHeight = 40;
        int iWidth = 140;
        
        //here the first tab starts
        Tab tab1 = new Tab();
        tab1.setText("GENERATE");
        tab1.setClosable(false);
        //content of the tab here
        GridPane grid1 = new GridPane();
        grid1.setPadding(new Insets(tGap,gap,gap,gap));
        grid1.setVgap(vGap);
        grid1.setHgap(gap);
        //place some nodes
        Label lblGrid1 = new Label(
                "To generate all necessary System Information, press on 'GENERATE'.\n"
                        + "Based on the Name the password is generated.");
        lblGrid1.setWrapText(true);
        lblGrid1.setId("lblText");
        Label lblGrid1Msg = new Label();
        lblGrid1Msg.setWrapText(true);
        lblGrid1Msg.setId("msgText_green");
        Label lblSysName1 = new Label("sysName:");
        lblSysName1.setMinWidth(uiWidth / 3);
        lblSysName1.setId("lblId");
        TextField txtSysName1 = new TextField();
        txtSysName1.setEditable(false);
        txtSysName1.setMinHeight(iHeight);
        txtSysName1.setId("txtId");
        Label lblUsrName1 = new Label("usrName:");
        lblUsrName1.setId("lblId");
        TextField txtUsrName1 = new TextField();
        txtUsrName1.setEditable(false);
        txtUsrName1.setMinHeight(iHeight);
        txtUsrName1.setId("txtId");
        Label lblUsrPass1 = new Label("usrPass:");
        lblUsrPass1.setId("lblId");
        TextField txtUsrPass1 = new TextField();
        txtUsrPass1.setEditable(false);
        txtUsrPass1.setMinHeight(iHeight);
        txtUsrPass1.setId("txtId");
        Button btGenerate = new Button("GENERATE");
        btGenerate.setOnAction(e -> {
            id = generateID();
            txtSysName1.setText("sys" + id);
            txtUsrName1.setText("usr" + id);           
            pw = generatePWFromID(id);
            txtUsrPass1.setText(pw);
            lblGrid1Msg.setId("msgText_green");
            //lblGrid1Msg.setText("credentials successfully generated");
        });
        btGenerate.setId("btGreen");
        btGenerate.setMinSize(iWidth, iHeight);
        GridPane.setConstraints(lblGrid1, 0, 0, 2, 1);
        GridPane.setConstraints(lblSysName1, 0, 1);
        GridPane.setConstraints(txtSysName1, 1, 1);
        GridPane.setConstraints(lblUsrName1, 0, 2);
        GridPane.setConstraints(txtUsrName1, 1, 2);
        GridPane.setConstraints(lblUsrPass1, 0, 3);
        GridPane.setConstraints(txtUsrPass1, 1, 3);
        GridPane.setConstraints(lblGrid1Msg, 0, 4, 2, 1);
        GridPane.setConstraints(btGenerate, 1, 5);
        grid1.getChildren().addAll(
                lblGrid1, 
                lblSysName1, 
                txtSysName1, 
                lblUsrName1, 
                txtUsrName1, 
                lblUsrPass1, 
                txtUsrPass1,  
                lblGrid1Msg,
                btGenerate);
        tab1.setContent(grid1);
        tabPane.getTabs().add(tab1);
        
        
        //here the second tab starts
        Tab tab2 = new Tab();
        tab2.setText("RESTORE");
        tab2.setClosable(false);
        //content of the tab here
        GridPane grid2 = new GridPane();
        tabPane.getTabs().add(tab2);
        grid2.setPadding(new Insets(tGap,gap,gap,gap));
        grid2.setVgap(vGap);
        grid2.setHgap(gap);
        //place some nodes
        Label lblGrid2 = new Label("Restore the default password by putting in the system or user information and pressing on 'RESTORE'.\n"
                + "(Note: only the numbers of the sys- or usrID are needed,\n"
                + "so put in '123456' instead of 'sys123456' or 'usr123456')");
        lblGrid2.setId("lblText");
        lblGrid2.setWrapText(true);
        Label lblGrid2Msg = new Label();
        lblGrid2Msg.setWrapText(true);
        Label lblSysName2 = new Label("ID:");
        lblSysName2.setMinWidth(uiWidth / 3);
        lblSysName2.setId("lblId");
        TextField txtSysName2 = new TextField();
        txtSysName2.setMinHeight(iHeight);
        txtSysName2.setId("txtId");
        Label lblUsrPass2 = new Label("usrPass:");
        lblUsrPass2.setId("lblId");
        TextField txtUsrPass2 = new TextField();
        txtUsrPass2.setEditable(false);
        txtUsrPass2.setMinHeight(iHeight);
        txtUsrPass2.setId("txtId");
        Button btCalculate = new Button("RESTORE");
        btCalculate.setOnAction(e -> {
            if((txtSysName2.getText().equals("")) || (txtSysName2.getText() == null)) {
                lblGrid2Msg.setId("msgText_red");
                lblGrid2Msg.setText("ID is empty!");
            } else {
                int id = parseID(txtSysName2.getText());
                if(id == 0) {
                    lblGrid2Msg.setId("msgText_red");
                    lblGrid2Msg.setText("ID is invalid!");
                } else {
                    String pw = generatePWFromID(id);
                    txtUsrPass2.setText(pw);
                    lblGrid2Msg.setId("msgText_green");
                    //lblGrid2Msg.setText("password restored");
                }
            }
            
        });
        btCalculate.setId("btGreen");
        btCalculate.setMinSize(iWidth, iHeight);
        GridPane.setConstraints(lblGrid2, 0, 0, 2, 1);
        GridPane.setConstraints(lblSysName2, 0, 1);
        GridPane.setConstraints(txtSysName2, 1, 1);
        GridPane.setConstraints(lblUsrPass2, 0, 2);
        GridPane.setConstraints(txtUsrPass2, 1, 2);
        GridPane.setConstraints(lblGrid2Msg, 1, 3);
        GridPane.setConstraints(btCalculate, 1, 4);
        grid2.getChildren().addAll(
                lblGrid2, 
                lblSysName2,
                txtSysName2, 
                lblUsrPass2, 
                txtUsrPass2, 
                lblGrid2Msg, 
                btCalculate);
        tab2.setContent(grid2);
        
        
        Tab tab3 = new Tab();
        tab3.setText("ABOUT");
        tab3.setClosable(false);
        GridPane grid3 = new GridPane();
        grid3.setAlignment(Pos.TOP_CENTER);
        tabPane.getTabs().add(tab3);
        grid3.setPadding(new Insets(tGap,gap,gap,gap));
        grid3.setVgap(vGap);
        grid3.setHgap(gap);
        ImageView imageView = new ImageView();
        Image imgGrid3 = new Image("jCredGen.png");
        imageView.setImage(imgGrid3);
        GridPane.setConstraints(imageView, 0, 0);
        Label lblGrid3 = new Label("Credentials Generator v" + version + "\n"
                + "Copyright 2017 Kay Jakobs\n\n"
                + "License: \nGNU Lesser General Public License (LGPL)");
        lblGrid3.setId("lblText");
        lblGrid3.setWrapText(true);
        Button btDonate = new Button("DONATE");
        String myUrl =  "https://paypal.me/KJakobs";
        btDonate.setOnAction(e -> {
            getHostServices().showDocument(myUrl);
        });
        btDonate.setId("btGreen");
        btDonate.setMinSize(iWidth, iHeight);
        GridPane.setConstraints(lblGrid3, 0, 1, 2, 1);
        GridPane.setConstraints(btDonate, 1, 2);
        grid3.getChildren().addAll(
            imageView, 
            lblGrid3,
            btDonate);
        tab3.setContent(grid3);
        
        
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    
    private int generateID() {
        int id;
        int max;
        int min;
        switch (idLength) {
            case 3:
                max = 999;
                min = 100;
                break;
            case 4: 
                max = 9999;
                min = 1000;
                break;
            case 5:
                max = 99999;
                min = 10000;
                break;
            case 6:
                max = 999999;
                min = 100000;
                break;
            default:
                max = 0;
                min = 0;
                break;
        }
        id = (int) (Math.random() * ((max - min + 1)) + min);
        return id;
    }
    
    
    private int parseID(String sID) {
        int id;
        try {
            id = Integer.parseInt(sID);
            System.out.println("ID = " + id);
        } catch (NumberFormatException nfe) {
            System.err.println(nfe);
            id = 0;
        }
        return id;
    }

    
    private String generatePWFromID(int id) {
        int multiplicator = 2;
        String pw = "";
        //1. multiplicate the int id with itself to get a bigger number
        id = id * multiplicator;
        //2. convert int to string
        pw = "" + id;
        //3. exchange some digits to letters
        //1 = i
        //3 = e
        //4 = a
        //5 = s
        //6 = b
        //9 = q
        //pw = pw.replace("-", "");
        pw = pw.replace("3", "e");
        pw = pw.replace("4", "a");
        pw = pw.replace("5", "s");
        pw = pw.replace("6", "b");
        pw = pw.replace("9", "q");
        return pw;
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    
}
